/*!
A graph link
*/
use crate::graph::{NodeId, Nodes};
use crate::tick::Tick;
use nalgebra::RealField;

/// A link in a force-layout graph
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Link<F: RealField> {
    /// A straight-spring link
    StraightSpring(StraightSpring<F>),
}

impl<F: RealField> Link<F> {
    /// Compute this link's effect this tick
    #[inline]
    pub fn tick(&self, tick: &Tick<F>, nodes: &mut Nodes<F>) {
        match self {
            Link::StraightSpring(spring) => spring.tick(tick, nodes),
        }
    }
}

/// A straight spring
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct StraightSpring<F: RealField> {
    /// The resting length of this spring
    pub resting_len: F,
    /// The spring coefficient of this spring
    pub spring_coeff: F,
    /// The ends of the spring
    pub ends: [NodeId; 2],
}

impl<F: RealField> StraightSpring<F> {
    /// Compute this spring's effect this tick
    #[inline]
    pub fn tick(&self, tick: &Tick<F>, nodes: &mut Nodes<F>) {
        let l = nodes[self.ends[0]];
        let r = nodes[self.ends[1]];
        let displacement = l.pos - r.pos;
        let distance = displacement.norm();
        if distance.is_zero() { return }
        let force_amp = self.resting_len - distance;
        let force = displacement * (force_amp / distance);
        nodes[self.ends[0]].push(tick, force);
        nodes[self.ends[1]].push(tick, -force);
    }
}
