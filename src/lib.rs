/*!
`amime` is a simple library for small, fast force-layout graphs, optimized for the very simple and very complex cases.
*/

#![deny(missing_docs)]

pub mod force;
pub mod graph;
pub mod tick;
pub mod link;
pub mod node;