/*!
A force-graph
*/
use crate::force::Force;
use crate::link::Link;
use crate::node::Node;
use crate::tick::Tick;
use nalgebra::RealField;
use std::ops::{Index, IndexMut, Deref, DerefMut};
use ref_cast::{RefCast};
use num::ToPrimitive;

/// A force-layout graph, stored with adjacency lists
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Graph<F: RealField> {
    /// The nodes in this graph
    nodes: Vec<Node<F>>,
    /// The links in this graph
    links: Vec<Link<F>>,
    /// The forces in this graph
    forces: Vec<Force<F>>,
}

impl<F: RealField> Default for Graph<F> {
    #[inline]
    fn default() -> Graph<F> {
        Graph::new()
    }
}

impl<F: RealField> Graph<F> {
    /// Create a new, empty graph
    #[inline]
    pub fn new() -> Graph<F> {
        Graph {
            nodes: Vec::new(),
            links: Vec::new(),
            forces: Vec::new(),
        }
    }
    /// Create a new, empty graph with a given capacity
    #[inline]
    pub fn with_capacity(nodes: usize, links: usize, forces: usize) -> Graph<F> {
        Graph {
            nodes: Vec::with_capacity(nodes),
            links: Vec::with_capacity(links),
            forces: Vec::with_capacity(forces),
        }
    }
    /// Add a new node to a graph
    #[inline]
    pub fn new_node(&mut self, node: Node<F>) -> NodeId {
        let ix = self.nodes.len();
        self.nodes.push(node);
        NodeId(ix as u32)
    }
    /// Add a new link to a graph
    #[inline]
    pub fn new_link(&mut self, link: Link<F>) -> LinkId {
        let ix = self.links.len();
        self.links.push(link);
        LinkId(ix as u32)
    }
    /// Get the nodes of this graph
    #[inline]
    pub fn nodes(&self) -> &Nodes<F> {
        RefCast::ref_cast(&self.nodes[..])
    }
    /// Mutably get the nodes of this graph
    #[inline]
    pub fn nodes_mut(&mut self) -> &mut Nodes<F> {
        RefCast::ref_cast_mut(&mut self.nodes[..])
    }
}

impl<F: RealField + ToPrimitive> Graph<F> {
    /// Tick this graph forwards
    #[inline]
    pub fn tick(&mut self, tick: &Tick<F>) {
        for link in self.links.iter() {
            link.tick(tick, RefCast::ref_cast_mut(&mut self.nodes[..]))
        }
        for force in self.forces.iter_mut() {
            force.tick(tick, RefCast::ref_cast_mut(&mut self.nodes[..]))
        }
    }
}

/// An identifier for a node in a force-layout graph
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct NodeId(u32);

impl NodeId {
    /// Get a unique node number
    #[inline]
    pub fn num(self) -> u32 {
        self.0
    }
}

/// An identifier for a link in a force-layout graph
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct LinkId(u32);

impl LinkId {
    /// Get a unique link number
    #[inline]
    pub fn num(self) -> u32 {
        self.0
    }
}

impl<F: RealField> Index<NodeId> for Graph<F> {
    type Output = Node<F>;
    #[inline]
    fn index(&self, n: NodeId) -> &Node<F> {
        &self.nodes[n.0 as usize]
    }
}

impl<F: RealField> IndexMut<NodeId> for Graph<F> {
    #[inline]
    fn index_mut(&mut self, n: NodeId) -> &mut Node<F> {
        &mut self.nodes[n.0 as usize]
    }
}


impl<F: RealField> Index<LinkId> for Graph<F> {
    type Output = Link<F>;
    #[inline]
    fn index(&self, n: LinkId) -> &Link<F> {
        &self.links[n.0 as usize]
    }
}

impl<F: RealField> IndexMut<LinkId> for Graph<F> {
    #[inline]
    fn index_mut(&mut self, n: LinkId) -> &mut Link<F> {
        &mut self.links[n.0 as usize]
    }
}

/// The nodes in a graph
#[derive(Debug, Eq, PartialEq, Hash, RefCast)]
#[repr(transparent)]
pub struct Nodes<F: RealField>(pub [Node<F>]);

impl<F: RealField> Deref for Nodes<F> {
    type Target = [Node<F>];
    #[inline]
    fn deref(&self) -> &[Node<F>] {
        &self.0
    }
}

impl<F: RealField> DerefMut for Nodes<F> {
    #[inline]
    fn deref_mut(&mut self) -> &mut [Node<F>] {
        &mut self.0
    }
}

impl<F: RealField> Index<NodeId> for Nodes<F> {
    type Output = Node<F>;
    #[inline]
    fn index(&self, n: NodeId) -> &Node<F> {
        &self.0[n.0 as usize]
    }
}

impl<F: RealField> IndexMut<NodeId> for Nodes<F> {
    #[inline]
    fn index_mut(&mut self, n: NodeId) -> &mut Node<F> {
        &mut self.0[n.0 as usize]
    }
}