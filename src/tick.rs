/*!
A graph tick
*/
use nalgebra::RealField;

/// A tick for a force-layout graph
#[derive(Debug, Clone, Eq, PartialEq, Default)]
pub struct Tick<F: RealField> {
    /// The amount of time elapsed during the tick
    pub dt: F,
}

impl<F: RealField> From<F> for Tick<F> {
    fn from(dt: F) -> Tick<F> {
        Tick { dt }
    }
}
