/*!
A graph node
*/
use crate::tick::Tick;
use nalgebra::{RealField, Vector2};

/// A node in a force-layout graph
#[derive(Debug, Copy, Clone, Eq, PartialEq, Default, Hash)]
pub struct Node<F: RealField> {
    /// The mass of this node
    pub mass: F,
    /// The friction decay coefficient of this node. `0.0` implies a pinned node
    pub decay: F,
    /// The position of this node on a 2D grid
    pub pos: Vector2<F>,
    /// The velocity of this node on a 2D grid
    pub vel: Vector2<F>,
}

impl<F: RealField> Node<F> {
    /// Get the momentum of this node
    pub fn momentum(&self) -> Vector2<F> {
        self.vel * self.mass
    }
    /// Get the energy of this node
    pub fn energy(&self) -> F {
        self.vel.norm() * self.mass
    }
    /// Get the x-coordinate of this node
    pub fn x(&self) -> F {
        self.pos[0]
    }
    /// Get the y-coordinate of this node
    pub fn y(&self) -> F {
        self.pos[1]
    }
    /// Get the x-velocity of this node
    pub fn vx(&self) -> F {
        self.pos[0]
    }
    /// Get the y-velocity of this node
    pub fn vy(&self) -> F {
        self.pos[1]
    }
    /// Accelerate this node for a tick
    #[inline]
    pub fn acc(&mut self, tick: &Tick<F>, acc: Vector2<F>) {
        self.vel += acc * tick.dt;
    }
    /// Push this node forwards for a tick
    #[inline]
    pub fn push(&mut self, tick: &Tick<F>, force: Vector2<F>) {
        let acc = force / self.mass;
        self.acc(tick, acc);
    }
}
