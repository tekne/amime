/*!
Particle repulsion and attraction with forces and charges
*/

use crate::graph::{NodeId, Nodes};
use crate::tick::Tick;
use fxhash::FxBuildHasher;
use indexmap::{map::Entry, IndexMap};
use nalgebra::{RealField, Vector2};
use num::NumCast;
use num::ToPrimitive;
use smallvec::SmallVec;

/// A force
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Force<F: RealField> {
    /// A force like a uniform electric charge on all particles
    Charge(Charge<F>),
}

impl<F: RealField + ToPrimitive> Force<F> {
    /// Compute this force's effect this tick
    #[inline]
    pub fn tick(&mut self, tick: &Tick<F>, nodes: &mut Nodes<F>) {
        match self {
            Force::Charge(charge) => charge.tick(tick, nodes),
        }
    }
}

/// A force like a uniform electric charge on all particles
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Charge<F: RealField> {
    /// The quadratic intensity of this force
    quadratic: F,
    /// The bin-size of this map
    bin_size: F,
    /// The particle-bins of this force
    bins: IndexMap<Vector2<i32>, Bin, FxBuildHasher>,
}

/// The size of a small force-bin. Beyond this size, the bin will allocate
pub const SMALL_BIN_SIZE: usize = 8;

/// A force-bin
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Bin {
    /// This particle bin's neighbors stored in clockwise order
    neighbors: [u32; 8],
    /// The members of this particle bin
    members: SmallVec<[NodeId; SMALL_BIN_SIZE]>,
}

/// A bin reassignment
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum Reassignment {
    /// Reassignment to neighbor 0-7
    Neighbor(u8),
    /// Reassignment to arbitrary position
    Far(Vector2<i32>),
    /// No movement needed
    Here,
}

impl Bin {
    /// Compute the bin reassignment for a given position vector
    pub fn reassign<F: RealField + ToPrimitive>(
        pos: Vector2<F>,
        size: F,
        coords: Vector2<i32>,
    ) -> Reassignment {
        let rounded_pos = Vector2::<i32>::new(
            NumCast::from(pos[0] / size).unwrap_or(0),
            NumCast::from(pos[1] / size).unwrap_or(0),
        );
        let displacement = rounded_pos - coords;
        match (displacement[0], displacement[1]) {
            (0, 0) => Reassignment::Here,
            (1, 0) => Reassignment::Neighbor(0),
            (1, 1) => Reassignment::Neighbor(1),
            (0, 1) => Reassignment::Neighbor(2),
            (-1, 1) => Reassignment::Neighbor(3),
            (-1, 0) => Reassignment::Neighbor(4),
            (-1, -1) => Reassignment::Neighbor(5),
            (0, -1) => Reassignment::Neighbor(6),
            (1, -1) => Reassignment::Neighbor(7),
            _ => Reassignment::Far(rounded_pos),
        }
    }
    /// Compute the offset vector for a neighbor number
    pub fn offset(ix: u8) -> Vector2<i32> {
        match ix {
            0 => Vector2::new(1, 0),
            1 => Vector2::new(1, 1),
            2 => Vector2::new(0, 1),
            3 => Vector2::new(-1, 1),
            4 => Vector2::new(-1, 0),
            5 => Vector2::new(-1, -1),
            6 => Vector2::new(0, -1),
            _ => Vector2::new(1, -1),
        }
    }
    /// Compute the opposite of a neighbor number
    pub fn opposite(ix: u8) -> u8 {
        (ix + 4) % 8
    }
}

impl<F: RealField + ToPrimitive> Charge<F> {
    /// Compute the interaction between two particles this tick
    #[inline]
    pub fn interact(&self, i: NodeId, j: NodeId, tick: &Tick<F>, nodes: &mut Nodes<F>) {
        let displacement = nodes[i].pos - nodes[j].pos;
        let distance = displacement.norm();
        if distance == F::zero() {
            return;
        }
        let force_amp = self.quadratic / (distance * distance);
        let force = displacement * (force_amp / distance);
        nodes[i].push(tick, force);
        nodes[j].push(tick, force);
    }
    /// Delete a bin, updating neighbors appropriately
    #[inline]
    pub fn delete_bin(&mut self, ix: u32) {
        unimplemented!("Bin deletion @{}", ix)
    }
    /// Get the bin at the given position, inserting one if necessary
    #[inline]
    pub fn get_bin(&mut self, pos: Vector2<i32>) -> u32 {
        let ix = match self.bins.entry(pos) {
            Entry::Occupied(o) => return o.index() as u32,
            Entry::Vacant(v) => {
                let ix = v.index();
                v.insert(Bin {
                    neighbors: [u32::MAX; 8],
                    members: SmallVec::new(),
                });
                ix
            }
        } as u32;
        for nix in 0..8 {
            if let Some(neighbor) = self.bins.get_index_of(&(Bin::offset(nix as u8) + pos)) {
                self.bin_mut(ix).neighbors[nix] = neighbor as u32;
            }
        }
        ix
    }
    /// Get a bin
    pub fn bin(&self, ix: u32) -> &Bin {
        self.bins.get_index(ix as usize).unwrap().1
    }
    /// Mutably get a bin
    pub fn bin_mut(&mut self, ix: u32) -> &mut Bin {
        self.bins.get_index_mut(ix as usize).unwrap().1
    }
    /// Compute this force's effect this tick
    #[inline]
    pub fn tick(&mut self, tick: &Tick<F>, nodes: &mut Nodes<F>) {
        // Interactions
        for (_, bin) in self.bins.iter() {
            for member in bin.members.iter() {
                // Intra-bin interactions
                for intra in bin.members.iter() {
                    self.interact(*member, *intra, tick, nodes);
                }
                // Inter-bin interactions
                for (_, neighbor_bin) in bin.neighbors[0..4] // Avoid double-interactions!
                    .iter()
                    .filter_map(|ix| self.bins.get_index(*ix as usize))
                {
                    for neighbor in neighbor_bin.members.iter() {
                        self.interact(*member, *neighbor, tick, nodes)
                    }
                }
            }
        }
        // Bin reassignment / emptying
        let shunt = 0;
        for ix in 0..self.bins.len() {
            let ix = (ix - shunt) as u32;
            let (coords, bin) = self.bins.get_index_mut(ix as usize).unwrap();
            let coords = *coords;
            let bin_len = bin.members.len();
            let mut bin_shunt = 0;
            // Reassign members of the bin as necessary
            for bin_ix in 0..bin_len {
                let bin_ix = bin_ix - bin_shunt;
                let node = self.bin(ix).members[bin_ix];
                let target = match Bin::reassign(nodes[node].pos, self.bin_size, coords) {
                    Reassignment::Here => continue,
                    Reassignment::Neighbor(n) => self.bin(ix).neighbors[n as usize],
                    Reassignment::Far(f) => self.get_bin(f),
                };
                self.bin_mut(ix).members.swap_remove(bin_ix);
                self.bin_mut(target).members.push(node);
                bin_shunt += 1;
            }
            /*
            // The bin has been emptied, so remove it
            if bin_shunt == bin_len {
                self.delete_bin(ix as u32);
                shunt += 1;
            }
            */
        }
    }
}
